import { api } from '$shared/api/todo'
import { tanstack } from '$shared/utils/tanstack'
import type { PageLoad } from './$types'

export const load: PageLoad = async ({ parent, fetch }) => {
	const { queryClient } = await parent()

	await queryClient.prefetchQuery({
		queryKey: [tanstack.todo],
		queryFn: () => api(fetch).fetchTodos()
	})
}
