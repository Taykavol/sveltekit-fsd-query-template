import { api } from '$shared/api/todo'
import { tanstack } from '$shared/utils/tanstack'
import type { PageLoad } from './$types'

export const load: PageLoad = async ({ parent, fetch }) => {
	const { queryClient } = await parent()

	// Do something with the query client
}
