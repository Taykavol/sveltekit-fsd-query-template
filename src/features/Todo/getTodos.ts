import { createQuery } from '@tanstack/svelte-query'
import { api } from '$shared/api/todo'
import { tanstack } from '$shared/utils/tanstack'

export const getTodos = () =>
	createQuery({
		queryKey: [tanstack.todo],
		queryFn: () => api().fetchTodos()
	})
