import type { Post } from './types'

// Важно: не использовать fetch!!!!
export const api = (customFetch = fetch) => ({
	fetchTodos: async () => {
		const data = await customFetch('https://dummyjson.com/todos')
		const json = await data.json()
		return json.todos
	},
	getPosts: async (limit: number) => {
		const response = await customFetch('https://jsonplaceholder.typicode.com/posts')
		const data = (await response.json()) as Post[]
		return data.filter((x) => x.id <= limit)
	},
	getPostById: async (id: number): Promise<Post> => {
		const response = await customFetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
		const data = (await response.json()) as Post
		return data
	}
})
