module.exports = {
	root: true,
	extends: ['eslint:recommended', 'plugin:@typescript-eslint/recommended', 'plugin:svelte/recommended', 'prettier', 'plugin:@conarti/feature-sliced/recommended'],
	parser: '@typescript-eslint/parser',
	plugins: ['@typescript-eslint'],
	parserOptions: {
		sourceType: 'module',
		ecmaVersion: 2020,
		extraFileExtensions: ['.svelte']
	},
	rules: {
		'@typescript-eslint/no-unused-vars': 'warn',
		"@conarti/feature-sliced/layers-slices": "warn",
		"@conarti/feature-sliced/absolute-relative": "error",
		"@conarti/feature-sliced/public-api": "error",
		'no-console': 'warn',	
		// 'css(unknownAtRules)': 'off'
	},
	
	env: {
		browser: true,
		es2017: true,
		node: true
	},
	overrides: [
		{
			files: ['*.svelte'],
			parser: 'svelte-eslint-parser',
			parserOptions: {
				parser: '@typescript-eslint/parser'
			}
		}
	]
};
